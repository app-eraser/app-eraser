const { removeApp } = require('../model/mainWindow');

document.addEventListener('drop', (event) => { 
	event.preventDefault(); 
	event.stopPropagation(); 

	for (const f of event.dataTransfer.files) { 
		removeApp(f.path);
	} 
}); 

document.addEventListener('dragover', (e) => { 
	e.preventDefault(); 
	e.stopPropagation(); 
}); 

document.addEventListener('dragenter', (event) => { 
}); 

document.addEventListener('dragleave', (event) => { 
});


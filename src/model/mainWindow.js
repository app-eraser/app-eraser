
const fs = require('fs/promises');
let exec = require('child_process').exec;
const sudo = require('sudo-prompt');
const { pathLocations } = require('../utils/pathLocations.js');

function appNameFromPath(path) {
  const pathArr = path.split('/');
  const appNameWithExt = pathArr[pathArr.length - 1];
  // remove .app extension
  return appNameWithExt.slice(0, appNameWithExt.length - 4)
}

async function getBundleIdentifier(appName) {
  const bundleId = await new Promise((resolve, reject) => {
    exec(`osascript -e 'id of app "${appName}"'`, (error, stdout, stderr) => {
      if(error) {
        console.error(error);
        reject(error);
      }
      // remove new line
      resolve(stdout.substring(0, stdout.length - 1));
  })
})
  
  return bundleId;
}


async function findAppFiles(appName) {
  try {
    const filesToRemove = new Set([]);
    const directoryFilesPromiseArr = pathLocations.map((pathLocation) => fs.readdir(pathLocation));
    const directoryFiles = await Promise.allSettled(directoryFilesPromiseArr);

    const bundleId = await getBundleIdentifier(appName);

    const appNameNorm = appName.toLowerCase().replace(' ', '');
    const bundleIdNorm = bundleId.toLowerCase().replace(' ', '')


    directoryFiles.forEach((dir, index) => {
      if(dir.status === 'fulfilled') {
        dir.value.forEach((dirFile) => {
          const dirFileNorm = dirFile.toLowerCase().replace(' ', '');
          if(
            dirFileNorm.includes(appNameNorm)
            || dirFileNorm.includes(bundleIdNorm)
            ){
            console.log(`${pathLocations[index]}/${dirFile}`);
            filesToRemove.add(`${pathLocations[index]}/${dirFile}`);
          }
        })
      }
    });

    // convert set to array
    return [...filesToRemove];
  } catch (err) {
    console.error(err);
  }
}

async function moveFilesToTrash(files) {
  console.log('About to move files to trash', files);

  var spOptions = {
    name: 'App Eraser',
  };

  const posixFile = `POSIX file \\"${files.join('\\", POSIX file \\"')}\\"`;

  sudo.exec(`osascript -e "tell application \\"Finder\\" to delete { ${posixFile} } "`, spOptions,
    function(error, stdout, stderr) {
      if (error) throw error;
      console.log('stdout: ' + stdout);
    }
  );
}

async function removeApp(appPath) {
  const appName = appNameFromPath(appPath);
  console.log(`About to remove app '${appName}'`);
  console.log(pathLocations);
  const appFiles = await findAppFiles(appName);
  console.log(appFiles);
  moveFilesToTrash(appFiles);
}

module.exports = {
  removeApp,
}
# App Eraser
A free and open source application for removing Mac OS apps and related files.

# Why?
App Eraser was created as a free and open source alternative to popular tools like AppCleaner.

Tools like these are designed to remove not only the .app files but also related files that are left behind
if you were to simply delete the app file.

The obvious advantage of an app like this is that you save more space after deleting an application.
# Development
## Prerequisites
* NodeJS

## Running the app
* Clone this project
* `npm i`
* `npm start`


